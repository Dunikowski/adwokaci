const gulp = require('gulp');
const sass= require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const rename      = require('gulp-rename');
//const watchSass = require("gulp-watch-sass");
const browserSync = require('browser-sync').create();
 const uglify = require("gulp-uglify");
 var pipeline = require('readable-stream').pipeline;
 //const babel = require("gulp-babel");
 const concat = require("gulp-concat");


const config_js_files= [
  
  './inc/assets/js/slick.js',
  './inc/assets/js/additional_bottoms.js',
 './inc/assets/js/myscript.js'
];
   gulp.task("compress", function() {
   
     return pipeline(
      gulp.src(config_js_files),
        concat('all.js'),
          rename({suffix: '.min'}),
       gulp.dest('./inc/assets/js/')
 );
      
   });

  gulp.task('sass', function(){
    
    return gulp.src('./inc/assets/scss/style.scss')
      .pipe(sass({outputStyle: 'compressed'})) // Using gulp-sass
      .pipe(sourcemaps.init())
      .pipe(autoprefixer({
        browsers: ['last 10 versions'],
        cascade: false
    }))
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('./'))
  });

gulp.task('default',['sass'],function(){
  browserSync.init({
    watchTask: true,
    proxy: "localhost/adwokaci"
});
});
gulp.watch('./inc/assets/scss/*.scss', ['sass']).on('change', browserSync.reload);
 gulp.watch('./*.php').on('change', browserSync.reload);
gulp.watch("./inc/assets/js/myscript.js", ['compress']).on('change', browserSync.reload);
