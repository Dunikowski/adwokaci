

  let additionalBottom = {};

  additionalBottom.init = function(i) {
    additionalBottom.aim = jQuery(i);
    if(additionalBottom.aim.length == 0){
        console.error('Error: ',i,'. Aim not find');
        return;
    }
    additionalBottom.wrapper = jQuery('.js-additionalBottom');
    if(additionalBottom.wrapper.length == 0){
        console.error('Error: Wrapper js-additionalBottom, not find');
        return;
    }
    additionalBottom.addBottom = additionalBottom.wrapper.find('>.add-bottom');
  }

  additionalBottom.count = function () {
    const i = additionalBottom.aim.find('.additional-bottom-item');
    return (2 + i.length);
  }

  additionalBottom.add = function(i) {

    const j = '<div class="w-file additional-bottom-item"><span class="wpcf7-form-control-wrap"><input type="file" name="zaloncznik-'+i+'" size="40" class="wpcf7-form-control wpcf7-file i-files" id="zaloncznik-'+i+'" accept=".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.ppt,.pptx,.odt" aria-invalid="false"></span><br><label for="zaloncznik-'+i+'">Załącznik</label><span class="remove-bottom"></span></div>';

    if (additionalBottom.aim.length > 0) {
      additionalBottom.wrapper.before(j);

    }
  };
  additionalBottom.remove = function() {
    if (additionalBottom.aim.length > 0) {
      const i = jQuery(additionalBottom.aim).find('.additional-bottom-item');
      if(i.length > 0){
          i[i.length - 1].remove();
      }

    }
    return;
  };
