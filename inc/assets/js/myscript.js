(function($) {
  let SuperAdwokaci = {}
  SuperAdwokaci.post_carousel = $('.carousel');
  SuperAdwokaci.fixed_menu_search_button = $('.fixed-menu');
  SuperAdwokaci.fixed_menu_button = SuperAdwokaci.fixed_menu_search_button.find('.fixed-menu-button');
  SuperAdwokaci.fixed_menu_search_button = SuperAdwokaci.fixed_menu_search_button.find('.search .botton-arrow');

  SuperAdwokaci.post_carousel.slick({
    lazyLoad: 'ondemand',
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    responsive: [{
      breakpoint: 1050,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    }]
  });

  SuperAdwokaci.init_scrolling_form = function(a) {

    const scrolling_form = $(a);

    if (scrolling_form.length == 0) return;

    $(window).on('scroll', function() {
      const _top = $(window).scrollTop();
      const _move_form = scrolling_form.find('.scrolling-form');
      const _move_form_h = _move_form.height();
      const _form_h = scrolling_form.height();
      let fixed_margin_top = 20;
      const _w = $(window).width();
      if (_w > 1679) {
        fixed_margin_top = 70;
      }
      if (_w < 1025) {
        fixed_margin_top = 70;
      }
      if (_form_h - _move_form_h - fixed_margin_top <= 0) return;
      if (_top + fixed_margin_top > scrolling_form.offset().top) {

        const _form_w = scrolling_form.width();
        if (_top + fixed_margin_top > scrolling_form.offset().top + _form_h - _move_form_h) {

          if (_move_form.hasClass('fixed-form')) {
            _move_form.removeClass('fixed-form').addClass('drop-form').css({
              'margin-top': _form_h - _move_form_h
            });

          }
          return;
        } else {
          if (_move_form.hasClass('fixed-form')) {
            return;
          } else {
            _move_form.addClass('fixed-form').css({
              "width": _form_w
            });
            if (_move_form.hasClass('drop-form')) {
              _move_form.removeClass('drop-form').css({
                'margin-top': 0
              });
            }
            return;
          }
        }

      } else {

        if (_move_form.hasClass('fixed-form')) {
          _move_form.removeClass('fixed-form');
        } else {
          return;
        }
      }
    });
  }

  SuperAdwokaci.fixed_menu_search_button.on('click', function(e) {
    e.stopPropagation();
    $(this).closest('.search').toggleClass('active');
    $(this).closest('.widget_search').find('.search-submit').prop('disabled', function(i, v) {
      return !v;
    })
  });

  SuperAdwokaci.fixed_menu_button.on('click', function(e) {
    e.stopPropagation();
    $(this).closest('.fixed-menu').toggleClass('hide-fixed-menu');
  });

  if ($(window).width() < 1025) {
    SuperAdwokaci.fixed_menu_search_button.click();
  }

  if ($(window).width() > 991) {
    SuperAdwokaci.init_scrolling_form('.js-scrolling-form');
  }


  $(window).on('resize', function() {

    if ($(window).width() < 1025) {
      SuperAdwokaci.fixed_menu_search_button.click();
      MobileMenu.init();
    }
    if ($(window).width() > 991) {
      SuperAdwokaci.init_scrolling_form('.js-scrolling-form');
    } else {
      const i = $('.js-scrolling-form').find('.scrolling-form');
      i.removeAttr('style').removeClass('fixed-form');
    }
  });

  let MobileMenu = {};
  MobileMenu.init = function() {
    // Add close to menu
    var _w = jQuery(window).width();

    var _nav = $('#main-nav');
    if (_nav.find("span.close").length == 0) {
      _nav.prepend("<span class='close'></span>");
    }
    var _span = _nav.find("span.close");
    if (_span.length > 0) {
      _span.on("click", function() {
        _nav.siblings('button.navbar-toggler').click();
      });
    }
  }

  if ($(window).width() < 1025) {
    MobileMenu.init();
  }


  // input file update
  let UpdateInputsTypeFile = {}


  UpdateInputsTypeFile.init = function(a) {
    if (a.length == 0) return;
    Array.prototype.forEach.call(a, function(input) {

      var label = jQuery(input).parent().siblings('label'),
        labelVal = label.innerHTML;

      jQuery(input).on('change', function(e) {
        var fileName = '';
        if (this.files && this.files.length > 1) {
          fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);

        } else {
          fileName = e.target.value.split('\\').pop();

        }

        if (fileName) {
          label[0].innerHTML = fileName;

        } else {
          label[0].innerHTML = labelVal;
        }

      });
    });
  }
  UpdateInputsTypeFile.init($('.i-files'));
  additionalBottom.init('.w-form .w-a-filile');
  additionalBottom.addBottom.on('click', function(e) {

    e.preventDefault();
    e.stopPropagation();

    if (additionalBottom.aim.length == 0) return;

    const i = additionalBottom.count();
    if ($('body').hasClass('page-template-contact')) {
      if (i > 7) return;
    } else {
      if (i > 5) return;
    }

    additionalBottom.add(i);
    UpdateInputsTypeFile.init($('.i-files'));

    additionalBottom.aim.find('.remove-bottom').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).closest('.additional-bottom-item').remove();
      UpdateInputsTypeFile.init($('.i-files'));
    });
  });

})(jQuery)