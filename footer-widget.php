<?php
$footer_address = get_field('adres',71);
$footer_nip = get_field('nip',71);
$footer_phones = get_field('telefon_biuro',71);
$footer_emai = get_field('adresy_email',71);
?>
        <div id="footer-widget" >
            <div class="w-content">
                <div class="row">
                
                    <div class="col-12 col-lg-3 col-xl-3">
                        <div class="logotype">
                            <img src="<?php echo get_template_directory_uri();?>'/images/logotyp.png'" alt="SuperAdwokaci">
                        </div>
                        <?php if($footer_address ):;?>
                        <p class="text"><?php echo trim($footer_address);?></p>
                        <?php endif;?>
                        <?php if($footer_nip):;?>
                        <p class="text"><span>NIP</span> <?php echo trim($footer_nip);?></p>
                        <?php endif;?>
                    </div>
                
                     <?php if ( is_active_sidebar( 'footer-1' )) : ?> 
                        <div class="col-12 col-lg-7 col-xl-6 menu-footer"><?php dynamic_sidebar( 'footer-1' ); ?></div>
                     <?php endif; ?>
                        
                    <div class="col-12 col-lg-2 col-xl-3">
                        <h2 class="widgettitle">Kontakt</h2>
                    <?php if($footer_phones):;?>
                        <div class="w-phones">
                            <a class="text" href="tel:<?php echo preg_replace('/(\s|-)/i','',$footer_phones);?>" rel="nofollow"> <span>tel:</span><?php echo trim($footer_phones);?></a>
                        </div>
                    <?php endif;?>
                    <?php if($footer_emai[0]):;?>
                        <div class="w-email">
                            <a class="text" href="mailto:<?php echo preg_replace('/(\s|-)/i','',$footer_emai[0]['adres']);?>" rel="nofollow"><?php echo trim($footer_emai[0]['adres']);?></a>
                        </div>
                    <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
<?php 