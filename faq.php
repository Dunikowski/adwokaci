<?php
/**
* Template Name: Faq
 */

$faq = get_field('najczesciej_zadawane_pytania');

get_header(); 	
?>

<section class="faq w-content">
  <?php
			
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
			?>
  <main id="main" class="site-main" role="main">

    <div class="w-content-faq">
      <h1 class="title"><span><?php the_title();?></span></h1>
      <?php if($faq):;?>
      <div class="w-colapse">
        <?php foreach($faq as $key => $row_faq):;?>
        <div class="item">
          <div class="w-heading" data-toggle="collapse" data-target="#collapseOne_<?php  echo $key;?>" aria-expanded="<?php echo ($key == 0)?'true':'false';?>" aria-controls="collapseOne">
            <span class="f-size-23 before-element">
              <?php echo $row_faq['pytanie']; ?>
            </span>
            <span class="botton-arrow "></span>
          </div>
          <div class="text collapse <?php echo ($key == 0)?'show':'';?>" id="collapseOne_<?php  echo $key;?>">
            <?php echo $row_faq['odpowiedz']; ?>
          </div>
        </div>
        <?php endforeach;?>
      </div>
      <?php endif;?>
    </div>

    <div class="w-form js-scrolling-form">
      <div class="scrolling-form">
        <p class="title">Formularz</p>
        <?php echo do_shortcode( '[contact-form-7 id="4" title="Formularz kontaktowy podstrony Oferta"]' );?>
      </div>
    </div>
    <div class="text-cta">
      <div class="w-text">
        <?php if($heading):;?>
        <p class="heading"><?php echo $heading;?></p>
        <?php endif;?>
        <?php if($text_help):;?>
        <p class="text"><?php echo $text_help;?></p>
        <?php endif;?>
      </div>

    </div>
  </main><!-- #main -->
</section><!-- offers -->

<?php
get_footer();