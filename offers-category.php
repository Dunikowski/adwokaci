<?php
/**
* Template Name: Offers-category
 */
$_section_offer= get_field('oferty_grupa');

get_header(); 	
?>

<section class="offers w-content">
  <?php
			
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
			?>
  <main id="main" class="site-main" role="main">

    <div class="w-content-ofer w-article">
      <h1 class="title"><span><?php the_title();?></span></h1>

      <?php if($_section_offer):;?>

      <div class="w-offers">

        <?php foreach ( $_section_offer as $row_ofer): ;?>
        <div class="article-item">
         
          <?php if($row_ofer['hiperlacze_oferty'] ):;?>
          <div class="info">
          <a href="<?php echo get_the_permalink($row_ofer['hiperlacze_oferty']);?>" rel="nofollow">
            <p class="headin-info">
              <?php echo get_the_title($row_ofer['hiperlacze_oferty']);?>
            </p>
          </a>
            <p class="text">
              <?php echo wp_trim_words(wp_filter_nohtml_kses(get_post_field('post_content', $row_ofer['hiperlacze_oferty'])),60);?>
            </p>
            <div class="w-cta">
              <a href="<?php echo get_the_permalink($row_ofer['hiperlacze_oferty']);?>" class="cta-arrow" rel="nofollow">Czytaj dalej
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="12px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
                  <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
        C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
        c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                  <g>
                  </g>
                </svg>
              </a>
            </div>
          </div>
          <?php endif;?>
        </div>

        <?php endforeach ;?>

        <?php endif;?>
      </div>
    </div>

    <div class="w-form js-scrolling-form">
      <div class="scrolling-form">
        <p class="title">Formularz</p>
        <?php echo do_shortcode( '[contact-form-7 id="4" title="Formularz kontaktowy podstrony Oferta"]' );?>
      </div>
    </div>

  </main><!-- #main -->
</section><!-- offers -->

<?php
get_footer();