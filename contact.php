<?php
/**
* Template Name: Contact
 */

$address = get_field('adres');
$nip = get_field('nip');
$phones = get_field('telefony');
$phones_ofice = get_field('telefon_biuro');
$emai = get_field('adresy_email');
get_header(); 	
?>

	<section  class="contact w-content">
	<?php
			
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
      }
      
      while ( have_posts() ) : the_post();

        echo '<h1 class="title"><span>';
          the_title();
        echo '</span></h1>';

      endwhile;// End of the loop. 
      ?>
      
		<main id="main" class="site-main" role="main">

		<div class="w-content-contact">
    <div class="info">
      <p class="headin-info-contact">Dane kontaktowe</p>
      <p class="text margin-top-50">Supra Adwokaci</p>
      <div class="w-data">
      <div class="w-address">
        
        <?php if($address):;?>
          <p class="text"><?php echo trim($address);?></p>
        <?php endif;?>
      </div>
      <p class="text">
      <?php if($nip):;?>
         <span>NIP</span> <?php echo trim($nip);?>
        <?php endif;?>
      </p>
      <?php if($phones || $phones_ofice):;?>
        <div class="w-phones">
        <?php if($phones_ofice):;?>
        <a class="text" href="tel:<?php echo preg_replace('/(\s|-)/i','',$phones_ofice);?>" rel="nofollow"> <span>Biuro tel:</span><?php echo trim($phones_ofice);?></a>

        <?php endif;?>
          <?php foreach($phones as $row):;?>
          <div class="phone-item text">
          <div class="w-position-name"><span class="position"><?php echo trim($row['tytul']);?>:</span><span class="name"><?php echo trim($row['imie_i_nazwisko']);?></span></div>
         
          <a class="text" href="tel:<?php echo preg_replace('/(\s|-)/i','',$row['numer_telefonu']);?>" rel="nofollow"> <span>tel:</span><?php echo trim($row['numer_telefonu']);?></a>
          </div>
          <?php endforeach;?>
        </div>
      <?php endif;?>
      <?php if($emai):;?>
        <div class="w-email">
          <?php foreach($emai as $row):;?>
          <a class="text" href="mailto:<?php echo preg_replace('/(\s|-)/i','',$row['adres']);?>" rel="nofollow"><?php echo trim($row['adres']);?></a>
          <?php endforeach;?>
        </div>
      <?php endif;?>
      </div>
      
      <div class="map margin-top-30">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2505.2557938396467!2d17.005147415913918!3d51.103740547957116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470fc21aa089189f%3A0x30d64409705ee57!2sT%C4%99czowa%2082c%2C%2053-603%20Wroc%C5%82aw!5e0!3m2!1sen!2spl!4v1572358099160!5m2!1sen!2spl" frameborder="0" style="border:0;" allowfullscreen=""></iframe></div>
    </div>
		<div class="w-form">
      <p class="headin-info-contact">Formularz</p>
        <?php echo do_shortcode( '[contact-form-7 id="263" title="Formularz podstrony kontakt"]' );?>
    </div>
		</div>
		</main><!-- #main -->
	</section><!-- offers -->

<?php
get_footer();
