<?php
/**
* Template Name: Full Width
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main w-content" role="main">

			<h1 class="title before-element"><?php
			the_title()
			?></h1>
			<div class="text">
			<?php
			the_content();
			?>
			</div>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
