<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */
$heading = get_field('naglowek',57);
$text_help = get_field('tekst_pomocy',57);
$to_form = get_field('hiperlacze_do_formularza',57);
$post_thumbnail = get_the_post_thumbnail($post->ID); 
get_header(); ?>
<div class="w-container">
<section class="w-content">

  <div class="w-content-post">
    <div class="post">
	  <?php while ( have_posts() ) : the_post();

				if($post_thumbnail){
					echo '<div class="post-thumbnail">'.$post_thumbnail.'</div>';
				}
				
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
				
				echo '<h1 class="title"><span>';
					the_title();
				echo '</span></h1>';
				echo '<div class="the_content-single text">';
					the_content();
				echo '</div>';
			
			?>
      <div class="text-cta margin-top-30">
        <div class="w-text">
          <?php if($heading):;?>
          <p class="heading"><?php echo $heading;?></p>
          <?php endif;?>
          <?php if($text_help):;?>
          <p class="text"><?php echo $text_help;?></p>
          <?php endif;?>
        </div>
        <div class="w-cta">
          <a href="#w-form" class="cta-arrow">
            Uzupełnij Formularz
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="12px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
              <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
          C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
          c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
            </svg>
          </a>
        </div>

      </div>
      <?php

				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
					
				endwhile; // End of the loop.
				?>
    </div>

	<div class="w-form js-scrolling-form" id="w-form">
      <div class="scrolling-form">
        <p class="title">Formularz</p>
        <?php echo do_shortcode( '[contact-form-7 id="4" title="Formularz kontaktowy podstrony Oferta"]' );?>
      </div>
    </div>
  </div>
  <div class="w-carousel w-article w-content">
<h2 class="title">Przeczytaj także</h2>
	<div class="carousel">
		<?php foreach(get_posts(array('category'=> 5,)) as $row_article):;?>
		<div class="item">
		<div class="article-item">
		<?php 
				$blog_article = get_field('front_page',$row_article->ID);
				;?>
		<div class="article-picture">
			<a href="<?php echo get_the_permalink($row_blog['artykul']);?>" rel="nofollow">
			<img src="<?php echo $blog_article['obraz_front_page']['url'];?>" alt="<?php echo $blog_article['obraz_front_page']['alt'];?>" />
			</a>

		</div>
		<div class="info">
			<p class="headin-info">
			<?php echo get_the_title($row_article->ID);?>
			</p>
			<p class="text">
			<?php echo wp_trim_words(wp_filter_nohtml_kses(get_the_content($row_article->ID)),20);?>
			</p>
			<div class="w-cta">
			<a href="<?php echo get_the_permalink($row_blog['artykul']);?>" class="cta-arrow" rel="nofollow">Czytaj dalej
				<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="12px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
				<path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
						C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
						c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				</svg>
			</a>
			</div>
		</div>
		</div>
		</div>
		
		<?php endforeach;?>
	</div>
  </div>
</section><!-- #primary -->
			</div>
<?php

get_footer();