<?php
/**
 * The template for displaying category pages
 */

get_header();?>

<section class="w-content">
  <main id="main" class="site-main" role="main">

    <?php if ( have_posts() ) : ?>

	<div class="content-main-site">
	<div class="w-article">
		<?php  
		if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
		}
		;?>
	<h1 class="title"><?php echo single_cat_title( '', false );?></h1>
      <?php while ( have_posts() ) : the_post();?>
      <div class="article-item">
        <?php 
			$blog_article = get_field('front_page',$row_blog['artykul']);
			;?>
        <div class="article-picture">
          <a href="<?php echo get_the_permalink($row_blog['artykul']);?>" rel="nofollow">
            <img src="<?php echo $blog_article['obraz_front_page']['url'];?>" alt="<?php echo $blog_article['obraz_front_page']['alt'];?>" />
          </a>

        </div>
        <div class="info">
          <a href="<?php echo get_the_permalink($row_blog['artykul']);?>" rel="nofollow">
            <p class="headin-info">
              <?php echo get_the_title();?>
            </p>
          </a>
          <p class="text">
          <?php echo wp_trim_words(wp_filter_nohtml_kses(get_the_content()),30);?>
          </p>
          <div class="w-cta">
            <a href="<?php echo get_the_permalink($row_blog['artykul']);?>" class="cta-arrow" rel="nofollow">Czytaj dalej
              <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="12px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
                <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
					C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
					c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
              </svg>
            </a>
          </div>
        </div>
      </div>

      <?php endwhile; ?>
	</div>
  <div class="w-form js-scrolling-form">
      <div class="scrolling-form">
        <p class="title">Formularz</p>
        <?php echo do_shortcode( '[contact-form-7 id="4" title="Formularz kontaktowy podstrony Oferta"]' );?>
      </div>
    </div>
	
    <?php
			
			
			the_posts_pagination( array('screen_reader_text' => ' ',));

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

  </main><!-- #main -->
</section>

<?php
// get_sidebar();
get_footer();