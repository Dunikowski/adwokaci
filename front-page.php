<?php
/**
* Template Name: Home
 */
get_header();

  $_section_offer= get_field('oferta');
  $_section_he_for_f = get_field('pomoc_dla_frankowiczow');
  $_section_article = get_posts(
    array(
      'numberposts'      => 2,
      'category'         => 5,
  'orderby'          => 'date',
  'order'            => 'DESC'));
  //get_field('artykuly',96);
  $_section_workers = get_field('pracownicy',155);
?>

<section class="w-offer w-content">
  <div class="info-column">
    <?php if($_section_offer['naglowek']):;?>
    <div class="heading-text">
      <?php  echo $_section_offer['naglowek']; ?>
    </div>
    <?php endif;?>
    <?php if($_section_offer['tytul_1']  || $_section_offer['tytul_2'] ):;?>
    <h2 class="title">
      <div class="w-content-title">
        <p>
          <span class="heading-2"><?php  echo $_section_offer['tytul_1']; ?></span>
        </p>
        <p>
          <span class="heading-2"><?php  echo $_section_offer['tytul_2']; ?></span>
        </p>

      </div>
    </h2>
    <?php endif;?>
    <?php if($_section_offer['text']):;?>
    <div class="text">
      <?php echo $_section_offer['text'];?>
    </div>
    <?php endif;?>
    
  </div>

  <div class="picture">
    <?php 
           if($_section_offer['grafika']){
            echo '<img src="'.$_section_offer['grafika']['url'].'" alt="'.$_section_offer['grafika']['alt'].'"/>';
           } 
           ;?>
  </div>
  <div class="offer-column">
    <h2 class="title">Oferta</h2>
    <?php 
    $_offers_page = get_field('oferty_grupa',7);
    if( $_offers_page):;
    
    ?>
    <div class="w-tiles">
      <?php foreach ( $_offers_page as $row_offer ): ;?>
      <a href="<?php echo get_the_permalink($row_offer['hiperlacze_oferty']);?>" class="tiles" rel="follow"><span><?php echo $row_offer['tytul_oferty'];?></span></a>
      <?php endforeach ;?>
      <?php endif;?>
    </div>
  </div>
  </div>
</section>

<div class="w-content-main-site">
  <div class="content-main-site w-content">
    <div class="left-column">
      <section class="w-help-for-f">

        <?php if( $_section_he_for_f ['tytul']):;?>
        <h2 class="title before-element">
          <?php  echo  $_section_he_for_f ['tytul']; ?>
        </h2>
        <?php endif;?>

        <?php if( $_section_he_for_f['naglowek']):;?>
        <div class="heading-text">
          <?php  echo  $_section_he_for_f['naglowek']; ?>
        </div>
        <?php endif;?>

        <?php if( $_section_he_for_f ['tekst']):;?>
        <div class="text">
          <?php  echo  $_section_he_for_f ['tekst']; ?>
        </div>
        <?php endif;?>

        <?php if( $_section_he_for_f ['oferta_pomocy']):;?>
        <div class="w-cta">

          <?php foreach ( $_section_he_for_f ['oferta_pomocy'] as $row_help ): ;?>
          <a href="<?php echo $row_help['hiperlacze_do_podstrony'];?>" class="cta-arrow modification" rel="follow">
            <?php echo $row_help ['tekst_na_przycisku'];?>
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="10px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
              <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
              C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
              c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
            </svg>
          </a>
          <?php endforeach ;?>

        </div>
        <?php endif;?>
      </section>
      <section class="w-blog-article">
        <?php if($_section_article):;?>

        <div class="w-article">
          <h2 class="title">Artykuły</h2>
          <?php foreach ( $_section_article as $row_blog): ;?>
          <div class="article-item">
          <?php 
          $blog_article = get_field('front_page',$row_blog->ID);
          ;?>
            <div class="article-picture">
              <a href="<?php echo get_the_permalink($row_blog->ID);?>" rel="nofollow">
                <img src="<?php echo $blog_article['obraz_front_page']['url'];?>" alt="<?php echo $blog_article['obraz_front_page']['alt'];?>" />
              </a>

            </div>
            <div class="info">
              <div>
              <a href="<?php echo get_the_permalink($row_blog->ID);?>" rel="nofollow">
              <p class="headin-info">
              
                <?php echo $row_blog->post_title;?>
              </p>
          </a>
              <p class="text">
              
                <?php echo wp_trim_words(wp_filter_nohtml_kses($row_blog->post_content),30);?> 
              </p>
              </div>
           
              <div class="w-cta">
                <a href="<?php echo get_the_permalink($row_blog->ID);?>" class="cta-arrow" rel="nofollow">Czytaj dalej
                  <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="12px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
                    <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
                C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
                c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                  </svg>
                </a>
              </div>
            </div>
          </div>

          <?php endforeach ;?>
          <a href="<?php echo get_field('hiperlacze_do_podstrony_blog');?>" class="cta-arrow" rel="nofollow">
            Przejdź do bloga
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="12px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
              <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
          C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
          c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
              <g>
              </g>
            </svg>
          </a>
          <?php endif;?>
        </div>

      </section>
      <section class="w-content-team">
        
        <p class="title before-element">Kancelaria</p>
        <div class="the_content-team text"><?php echo get_post_field('post_content', 155);?></div>
        <p class="title before-element f-size-23">Zespół</p>
        <div class="w-workers">
          <?php foreach($_section_workers as $row_w):?>

          <div class="item-w">
            <div class="top">
              <div class="w-img">
                <a href="<?php echo get_field('hiperlacze_do_podstrony_zespol',155);;?>" rel="nofollow">
                <img src="<?php echo $row_w['zdjecie']['url'];?>" alt="<?php echo $row_w['zdjecie']['alt'];?>">
              </a>
                
              </div>
              <div class="w-position-name">
                <p class="position"><?php echo $row_w['stanowisko'];?></p>
                <p class="name"><?php echo $row_w['imie_i_nazwisko'];?></p>
              </div>
            </div>
          </div>

          <?php endforeach ;?>
        </div>
          </section>
         
    </div>

    <div class="w-form js-scrolling-form">
      <div class="scrolling-form">
        <p class="title">Formularz</p>
        <?php echo do_shortcode( '[contact-form-7 id="4" title="Formularz kontaktowy podstrony Oferta"]' );?>
      </div>
    </div>
  </div>
</div>

<?php

get_footer();