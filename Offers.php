<?php
/**
* Template Name: Offers
 */

$heading = get_field('naglowek',57);
$text_help = get_field('tekst_pomocy',57);
$to_form = get_field('hiperlacze_do_formularza',57);
get_header(); 	
?>

	<section  class="offers w-content">
	<?php
			
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
			?>
		<main id="main" class="site-main" role="main">

		<div class="w-content-ofer">
			<?php while ( have_posts() ) : the_post();

				echo '<h1 class="title"><span>';
					the_title();
				echo '</span></h1>';
				echo '<div class="the_content-offer text">';
					the_content();
				echo '</div>';

				endwhile; // End of the loop.
			?>
		</div>
			
    <div class="w-form js-scrolling-form" id="w-form">
      <div class="scrolling-form">
        <p class="title">Formularz</p>
        <?php echo do_shortcode( '[contact-form-7 id="4" title="Formularz kontaktowy podstrony Oferta"]' );?>
      </div>
    </div>
<div class="text-cta">
	<div class="w-text">
		<?php if($heading):;?>
			<p class="heading"><?php echo $heading;?></p>
		<?php endif;?>
		<?php if($text_help):;?>
			<p class="text"><?php echo $text_help;?></p>
		<?php endif;?>
	</div>
	<div class="w-cta">
	<a href="#w-form" class="cta-arrow">
		Uzupełnij Formularz
		<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width= "24px" height="12px"
          viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
        <path  d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
          C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
          c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        </svg>
	</a>
	</div>
	
</div>
		</main><!-- #main -->
	</section><!-- offers -->

<?php
get_footer();
