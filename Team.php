<?php
/**
* Template Name: Team
 */

$section_workers = get_field('pracownicy',155);

get_header(); 	
?>

	<section  class="w-content pt">
		<div class="w-content-team">
			 
    <?php
			
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
			?>
        <h1 class="title"><span><?php the_title();?></span></h1>
        <p class="title before-element">Kancelaria</p>
				<div class="the_content-team text"><?php the_content();?></div>
        <div class="w-workers">
          <?php foreach($section_workers as $row_w):?>
        
          <div class="item-w">
            <div class="top">
              <div class="w-img">
                <img src="<?php echo $row_w['zdjecie']['url'];?>" alt="<?php echo $row_w['zdjecie']['alt'];?>">
              </div>
              <div class="w-position-name">
                <p class="position"><?php echo $row_w['stanowisko'];?></p>
                <p class="name"><?php echo $row_w['imie_i_nazwisko'];?></p>
                <a class="text" href="tel:<?php echo preg_replace('/(\s|-)/i','',$row_w['telefon']);?>" rel="nofollow"> <span>tel: </span><?php echo trim($row_w['telefon']);?></a>
              </div>
            </div>
            <div class="description text">
            <?php echo $row_w['informacja_o_pracowniku'];?>
            </div>
          </div>
        
          <?php endforeach ;?>
        </div>
		</div>
			
    <div class="w-form js-scrolling-form">
      <div class="scrolling-form">
        <p class="title">Formularz</p>
        <?php echo do_shortcode( '[contact-form-7 id="4" title="Formularz kontaktowy podstrony Oferta"]' );?>
      </div>
    </div>
	</section>
<?php
get_footer();
